# Events Injector

Script that publishes sample messages to MQTT to test the new application.

The data to be published is located in the [data folder](data) and is compliant with the proto contracts.

The destination topics are the ones specified in the proto files.

## How to execute it

- Install Python requirements with:

    ```
    pip install -r requirements.txt
    ```
- Configure the data to inject in the [data folder](data).
- Execute it

    ```
    ./events-injector/main.py [--host=127.0.0.1] [--port=1883]
    ```

## Python models

The [Python models](model) that this script uses have been generated from the provided proto messages using the `protoc` compiler:

```
protoc --proto_path=proto --python_out=events-injector/model \
    proto/cabify/input/driver_rated.proto \
    proto/cabify/input/driver_tipped.proto \
    proto/cabify/input/journey_finished.proto
```
