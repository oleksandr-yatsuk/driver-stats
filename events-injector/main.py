#!/usr/bin/env python3

import json
import logging
import os
from threading import Thread

from google.protobuf import json_format
from google.protobuf.message import Message
from paho.mqtt import client as mqtt_client
from argparse import ArgumentParser
import model.cabify.input.driver_rated_pb2 as driver_rated
import model.cabify.input.driver_tipped_pb2 as driver_tipped
import model.cabify.input.journey_finished_pb2 as journey_finished

logging.basicConfig(level=logging.INFO, format='%(asctime)s [%(levelname)s] %(message)s')


class ModelMQTTPublisher(Thread):
    """
    Thread that publishes the canned data for a model into its associated MQTT topic
    """

    def __init__(self, client: mqtt_client.Client, model: str, msg: Message):
        """
        :param client: MQTT client
        :param model: The name of the model to process
        :param msg: Associated proto message for the model
        """
        super().__init__()
        self.model = model
        self.msg = msg
        self.client = client

    def run(self):
        project_folder = os.path.dirname(os.path.abspath(__file__))

        with open(os.path.join(project_folder, "data", f"{self.model}.json")) as f:
            events = json.load(f)
            topic = f"input/{self.model}"

            for num, event in enumerate(events):
                msg = json_format.ParseDict(event, self.msg, ignore_unknown_fields=False)
                result, mid = self.client.publish(topic, msg.SerializeToString())

                if result == 0:
                    logging.info(f"Message #{num} published to topic {topic}")
                else:
                    logging.error(f"Failed to published message to topic {topic}")


# Configure CLI parser
parser = ArgumentParser()
parser.add_argument("--host", dest="host", help="Host of the MQTT server", default="localhost")
parser.add_argument("--port", dest="port", help="Port of the MQTT server", default=1883, type=int)
args = parser.parse_args()

# Initialize MQTT client
msg_client = mqtt_client.Client("events-injector")
msg_client.connect(args.host, args.port)
msg_client.loop_start()

# Configure the threads with the canned data and launch them
threads = [
    ModelMQTTPublisher(msg_client, "journey_finished", journey_finished.JourneyFinished()),
    ModelMQTTPublisher(msg_client, "driver_rated", driver_rated.DriverRated()),
    ModelMQTTPublisher(msg_client, "driver_tipped", driver_tipped.DriverTipped()),
]

for thread in threads:
    thread.start()

for thread in threads:
    thread.join()

msg_client.disconnect()

logging.info("Process finished!")
