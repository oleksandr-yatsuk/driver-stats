# Data Engineering Code Challenge

## Context

At Cabify, we generate a lot of data as part of our ride hailing business.
This data flows through an event-oriented architecture in order to decouple the hundreds of microservices we run in production.

Some of these events are used by the Payments team to pay our driver partners according to some business rules, but they need help: their infrastructure is not capable of handling the throughput of events we currently have.

So, they have asked you to develop a streaming application to aggregate these events and compute a set of statistics they can use as input to emit the payments.

## Task

The application to develop should consume an endless stream of different events, compute the stats and publish them.
Intervals are non-overlapping ranges of time with fixed duration, for which the stats should be computed.

In this particular case, we use [MQTT](https://mqtt.org) as the messaging protocol using a dedicated topic per event.
Both input and output data is serialized using [Protocol Buffers](https://developers.google.com/protocol-buffers).
The different input messages are: [DriverRated](proto/cabify/input/driver_rated.proto), [DriverTipped](proto/cabify/input/driver_tipped.proto), [JourneyFinished](proto/cabify/input/journey_finished.proto).
The output event is [DriverStats](proto/cabify/output/driver_stats.proto).

All the messages are self-documented, so you will find more info about them in the source code, including the name of the MQTT topics and how the stats should be computed.

## Requirements

- The interval duration should be configurable at application level, with a default of 10 minutes. The application should emit a single event for each driver and interval.
- Finished journeys must be assigned to intervals using the `ended_at` field (not the `started_at` field).
- The stats for an interval should be output as soon as the interval has been closed (the definition of closed is up to you) 
- If there is no data from a driver in a specific interval, no event should be generated.
- The application should cope with a high throughput of input events (we are continuously growing our business).

## Tooling

There are some tools available that can help you in the challenge (you can modify them in any way):

- A [docker-compose.yml](docker-compose.yml) file with an instance of [Eclipse Mosquitto](https://mosquitto.org).
- An [events-injector utility](events-injector) to publish canned events to the MQTT topics.
- A ready-to-use CI system. As this repo is hosted in Gitlab, you can leverage its integrated CI to help you with testing, building or whatever you may need. Check the [.gitlab-ci.yml](.gitlab-ci.yml) for more info.

## Additional considerations

- We consider a Data Engineer as a Software Engineer that knows their way around data, so universal software engineering good practices/patterns must be applied.
- You are free to use whatever programming language or framework you deem is best to solve the problem, but please bear in mind we want to see your best!
- Please design the application and document your decisions the same way you would do for any other deliverable. We want to see how you operate in a quasi real work environment.

## Feedback

At Cabify, we really appreciate your interest and your time. We are highly interested on improving our Challenge and the way we evaluate our candidates.
Hence, we would like to beg five more minutes of your time to fill the following survey:

https://forms.gle/6NLZ4g4rahZ7PtQcA

Your opinion is really important. Thanks for your contribution!
