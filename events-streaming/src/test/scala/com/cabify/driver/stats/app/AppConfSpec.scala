package com.cabify.driver.stats.app

import com.cabify.driver.stats.app.conf.AppConf
import com.cabify.driver.stats.base.BaseSpec

import scala.concurrent.duration.Duration

class AppConfSpec extends BaseSpec {
  private val args = Seq(
    "--kafka-host",
    "kafka-host-somewhere",
    "--kafka-port",
    "112233",
    "--journey-finished-topic",
    "cab-journey-finished",
    "--driver-rated-topic",
    "cab-driver-was-rated",
    "--driver-tipped-topic",
    "cab-driver-was-tipped",
    "--driver-tips-allowed-delay-seconds",
    "4477",
    "--driver-rating-allowed-delay-seconds",
    "7799",
    "--driver-stats-interval-seconds",
    "7865",
    "--driver-stats-topic",
    "driver-statistics",
    "--checkpoint-location",
    "s3://cabify-streaming/driver-stats"
  )
  private val appName = "test-app"

  "kafka host" should "be parsed" in {
    val parsed = AppConf.parse(args, appName)

    val config = parsed.get

    config.kafka.host shouldBe "kafka-host-somewhere"
  }

  "kafka host" should "be required" in {
    AppConf.parse(removeArg("--kafka-host"), appName) shouldBe None
  }

  "kafka port" should "be parsed" in {
    val parsed = AppConf.parse(args, appName)

    val config = parsed.get

    config.kafka.port shouldBe 112233
  }

  "kafka port" should "be required" in {
    AppConf.parse(removeArg("--kafka-port"), appName) shouldBe None
  }

  "journey finished topic" should "be parsed" in {
    val parsed = AppConf.parse(args, appName)

    val config = parsed.get

    config.input.journeyFinished.topic shouldBe "cab-journey-finished"
  }

  "journey finished topic conf" should "have defaults" in {
    val parsed = AppConf.parse(args, appName)

    val config = parsed.get

    config.input.journeyFinished.includeHeaders shouldBe true
    config.input.journeyFinished.startingOffsets shouldBe "earliest"
    config.input.journeyFinished.failOnDataLoss shouldBe false
  }

  "journey finished topic" should "be required" in {
    AppConf.parse(removeArg("--journey-finished-topic"), appName) shouldBe None
  }

  "driver rated topic" should "be parsed" in {
    val parsed = AppConf.parse(args, appName)

    val config = parsed.get

    config.input.driverRated.topic shouldBe "cab-driver-was-rated"
  }

  "driver rated topic conf" should "have defaults" in {
    val parsed = AppConf.parse(args, appName)

    val config = parsed.get

    config.input.driverRated.includeHeaders shouldBe true
    config.input.driverRated.startingOffsets shouldBe "earliest"
    config.input.driverRated.failOnDataLoss shouldBe false
  }

  "driver rated topic" should "be required" in {
    AppConf.parse(removeArg("--driver-rated-topic"), appName) shouldBe None
  }

  "driver tipped topic" should "be parsed" in {
    val parsed = AppConf.parse(args, appName)

    val config = parsed.get

    config.input.driverTipped.topic shouldBe "cab-driver-was-tipped"
  }

  "driver tipped topic conf" should "have defaults" in {
    val parsed = AppConf.parse(args, appName)

    val config = parsed.get

    config.input.driverTipped.includeHeaders shouldBe true
    config.input.driverTipped.startingOffsets shouldBe "earliest"
    config.input.driverTipped.failOnDataLoss shouldBe false
  }

  "driver tipped topic" should "be required" in {
    AppConf.parse(removeArg("--driver-tipped-topic"), appName) shouldBe None
  }

  "driver stats interval" should "be specified in seconds" in {
    val parsed = AppConf.parse(args, appName)

    val config = parsed.get

    config.driverStatsInterval shouldBe Duration("7865 seconds")
  }

  "driver tips allowed delay" should "be specified in seconds" in {
    val parsed = AppConf.parse(args, appName)

    val config = parsed.get

    config.driverTipsAllowedDelay shouldBe Duration("4477 seconds")
  }

  "driver tips allowed delay" should "be required" in {
    val parsed = AppConf.parse(
      removeArg("--driver-tips-allowed-delay-seconds"),
      appName,
      failOnUnknownArg = true
    )

    parsed shouldBe None
  }

  "driver rating allowed delay" should "be specified in seconds" in {
    val parsed = AppConf.parse(args, appName)

    val config = parsed.get

    config.driverRatingAllowedDelay shouldBe Duration("7799 seconds")
  }

  "driver rating allowed delay" should "be required" in {
    val parsed = AppConf.parse(
      removeArg("--driver-rating-allowed-delay-seconds"),
      appName,
      failOnUnknownArg = true
    )

    parsed shouldBe None
  }

  "driver stats interval" should "be optional and have default value" in {
    val parsed = AppConf.parse(removeArg("--driver-stats-interval-seconds"), appName)

    val config = parsed.get

    config.driverStatsInterval shouldBe Duration("10 minutes")
  }

  "driver stats topic" should "be parsed" in {
    val parsed = AppConf.parse(args, appName)

    val config = parsed.get

    config.driverStatsTopic shouldBe "driver-statistics"
  }

  "driver stats topic" should "be required" in {
    AppConf.parse(removeArg("--driver-stats-topic"), appName) shouldBe None
  }

  "checkpoint location" should "be parsed" in {
    val parsed = AppConf.parse(args, appName)

    val config = parsed.get

    config.checkpointLocation shouldBe "s3://cabify-streaming/driver-stats"
  }

  "checkpoint location" should "be required" in {
    AppConf.parse(removeArg("--checkpoint-location"), appName) shouldBe None
  }

  private def removeArg(key: String): Seq[String] = {
    val index = args.indexOf(key)

    args.slice(0, index) ++ args.slice(index + 2, args.length)
  }
}
