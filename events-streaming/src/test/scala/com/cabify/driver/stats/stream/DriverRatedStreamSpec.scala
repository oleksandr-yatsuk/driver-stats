package com.cabify.driver.stats.stream

import cabify.input.DriverRated
import com.cabify.driver.stats.base.BaseSparkSpec
import com.cabify.driver.stats.kafka.KafkaEvent
import com.cabify.driver.stats.stream.DriverRatedStream.ratedAtPath
import com.google.protobuf.timestamp.Timestamp

import scala.concurrent.duration.Duration

class DriverRatedStreamSpec extends BaseSparkSpec with KafkaStreamMock {
  "kafka protobuf events" should "be converted to driver rated events" in {
    val kafkaEvents = Seq(
      KafkaEvent(
        DriverRated("a", "b", "1", Some(Timestamp(1647436076)), 5).toByteArray
      ),
      KafkaEvent(
        DriverRated("c", "d", "2", Some(Timestamp(1647436063)), 2).toByteArray
      )
    )
    val expected = Seq(
      DriverRatedEvent("a", "b", "1", Some(instant("2022-03-16T13:07:56Z")), 5),
      DriverRatedEvent("c", "d", "2", Some(instant("2022-03-16T13:07:43Z")), 2)
    )
    val (kafkaStream, memoryStream) = kafkaSource
    memoryStream.addData(kafkaEvents)

    val driverRatedStream = new DriverRatedStream(kafkaStream, Duration("1 minute"))

    val events = driverRatedStream.events

    val actual = readAllEvents(events)

    actual should have length expected.size
    actual should contain theSameElementsAs expected
  }

  "rated at timestamp" should "be converted to UTC local date time" in {
    val kafkaEvents = Seq(
      KafkaEvent(
        DriverRated("a", "b", "1", Some(Timestamp(1647414476)), 4).toByteArray
      ),
      KafkaEvent(
        DriverRated("c", "d", "2", Some(Timestamp(1612226049)), 10).toByteArray
      )
    )
    val expected = Seq(
      DriverRatedEvent("a", "b", "1", Some(instant("2022-03-16T13:07:56+06:00")), 4),
      DriverRatedEvent("c", "d", "2", Some(instant("2021-02-01T22:34:09-02:00")), 10)
    )

    val (kafkaStream, memoryStream) = kafkaSource
    memoryStream.addData(kafkaEvents)

    val driverRatedStream = new DriverRatedStream(kafkaStream, Duration("1 minute"))

    val events = driverRatedStream.events

    val actual = readAllEvents(events)

    actual should have length expected.size
    actual should contain theSameElementsAs expected
  }

  "event timestamp watermark" should "be set on ratedAt field" in {
    val watermarkThreshold = Duration("5543 minutes")
    val (kafkaStream, _)   = kafkaSource

    val driverRatedStream = new DriverRatedStream(kafkaStream, watermarkThreshold)

    val events = driverRatedStream.events

    val actualWatermark = watermarkField(ratedAtPath, events)

    actualWatermark shouldBe watermarkThreshold
  }
}
