package com.cabify.driver.stats.stream

import cabify.input.FinishReason.{ADMIN_CANCEL, DRIVER_CANCEL, DROP_OFF, RIDER_CANCEL}
import cabify.input.JourneyFinished
import com.cabify.driver.stats.base.BaseSparkSpec
import com.cabify.driver.stats.kafka.KafkaEvent
import com.cabify.driver.stats.stream.JourneyFinishedStream.endedAtPath
import com.google.protobuf.timestamp.Timestamp

import scala.concurrent.duration.Duration

class JourneyFinishedStreamSpec extends BaseSparkSpec with KafkaStreamMock {
  "kafka protobuf events" should "be converted to driver rated events" in {
    val kafkaEvents = Seq(
      KafkaEvent(
        JourneyFinished("a", "b", "c", DROP_OFF, Some(Timestamp(1689499423)), Some(Timestamp(1689499689)), 1200, 250).toByteArray
      ),
      KafkaEvent(
        JourneyFinished("d", "e", "f", DRIVER_CANCEL, Some(Timestamp(1688214101)), Some(Timestamp(1688324983)), 4432, 445).toByteArray
      ),
      KafkaEvent(
        JourneyFinished("g", "h", "i", RIDER_CANCEL, Some(Timestamp(1688142406)), Some(Timestamp(1688306684)), 33, 22).toByteArray
      ),
      KafkaEvent(
        JourneyFinished("j", "k", "l", ADMIN_CANCEL, Some(Timestamp(1669118940)), Some(Timestamp(1669126380)), 456, 3355).toByteArray
      )
    )
    val expected = Seq(
      JourneyFinishedEvent(
        "a",
        "b",
        "c",
        DROP_OFF.name,
        Some(instant("2023-07-16T09:23:43Z")),
        Some(instant("2023-07-16T09:28:09Z")),
        1200,
        250
      ),
      JourneyFinishedEvent(
        "d",
        "e",
        "f",
        DRIVER_CANCEL.name,
        Some(instant("2023-07-01T12:21:41Z")),
        Some(instant("2023-07-02T19:09:43Z")),
        4432,
        445
      ),
      JourneyFinishedEvent(
        "g",
        "h",
        "i",
        RIDER_CANCEL.name,
        Some(instant("2023-06-30T16:26:46Z")),
        Some(instant("2023-07-02T14:04:44Z")),
        33,
        22
      ),
      JourneyFinishedEvent(
        "j",
        "k",
        "l",
        ADMIN_CANCEL.name,
        Some(instant("2022-11-22T12:09:00Z")),
        Some(instant("2022-11-22T14:13:00Z")),
        456,
        3355
      )
    )
    val (kafkaStream, memoryStream) = kafkaSource
    memoryStream.addData(kafkaEvents)

    val journeyFinishedStream = new JourneyFinishedStream(kafkaStream, Duration("1 minute"))

    val events = journeyFinishedStream.events

    val actual = readAllEvents(events)

    actual should have length expected.size
    actual should contain theSameElementsAs expected
  }

  "journey ended at timestamp" should "be converted to UTC local date time" in {
    val kafkaEvents = Seq(
      KafkaEvent(
        JourneyFinished("a", "b", "c", DROP_OFF, Some(Timestamp(1689499423)), Some(Timestamp(1689499689)), 1200, 250).toByteArray
      )
    )
    val expected = Seq(
      JourneyFinishedEvent(
        "a",
        "b",
        "c",
        DROP_OFF.name,
        Some(instant("2023-07-16T09:23:43Z")),
        Some(instant("2023-07-16T09:28:09Z")),
        1200,
        250
      )
    )
    val (kafkaStream, memoryStream) = kafkaSource
    memoryStream.addData(kafkaEvents)

    val journeyFinishedStream = new JourneyFinishedStream(kafkaStream, Duration("1 minute"))

    val events = journeyFinishedStream.events

    val actual = readAllEvents(events)

    actual should have length expected.size
    actual should contain theSameElementsAs expected
  }

  "event timestamp watermark" should "be set on endedAt field" in {
    val watermarkThreshold = Duration("332 seconds")
    val (kafkaStream, _)   = kafkaSource

    val journeyFinishedStream = new JourneyFinishedStream(kafkaStream, watermarkThreshold)

    val events = journeyFinishedStream.events

    val actualWatermark = watermarkField(endedAtPath, events)

    actualWatermark shouldBe watermarkThreshold
  }
}
