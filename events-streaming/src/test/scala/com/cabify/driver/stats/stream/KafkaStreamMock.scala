package com.cabify.driver.stats.stream

import com.cabify.driver.stats.kafka.{KafkaEvent, KafkaServerConf, KafkaStreamSource, KafkaTopicConf}
import org.apache.spark.sql.Encoders.product
import org.apache.spark.sql.execution.streaming.MemoryStream
import org.apache.spark.sql.{Dataset, Encoder, SQLContext, SparkSession}
import org.scalamock.scalatest.MockFactory

import java.util.UUID.randomUUID
import scala.concurrent.duration.Duration
import scala.reflect.runtime.universe.TypeTag

trait KafkaStreamMock extends MockFactory {
  private val kafka      = KafkaServerConf("stub", 0)
  private val kafkaTopic = KafkaTopicConf("stub-topic")

  class KafkaStreamSourceStub extends KafkaStreamSource(kafka, kafkaTopic)(null)

  protected def kafkaSource(implicit spark: SQLContext): (KafkaStreamSource, MemoryStream[KafkaEvent]) = {
    implicit val eventEncoder: Encoder[KafkaEvent] = product[KafkaEvent]

    val memoryStream = MemoryStream[KafkaEvent]

    val kafkaSource = stub[KafkaStreamSourceStub]
    (kafkaSource.stream _).when().returns(memoryStream.toDS)

    (kafkaSource, memoryStream)
  }

  protected def readAllEvents[E <: Product: TypeTag](events: Dataset[E], queryName: Option[String] = None)(implicit spark: SparkSession): Array[E] = {
    implicit val encoder: Encoder[E] = product[E]

    val queryNameOrRandom = queryName.getOrElse(randomQueryName)

    val query = events.writeStream
      .queryName(queryNameOrRandom)
      .format("memory")
      .outputMode("append")
      .start()

    query.processAllAvailable

    spark
      .sql(s"SELECT * FROM $queryNameOrRandom")
      .as[E]
      .collect()
  }

  protected def watermarkField(field: String, dataset: Dataset[_]): Duration = {
    val fieldIndex = dataset.schema.fieldIndex(field)
    val watermark  = dataset.schema(fieldIndex).metadata.getLong("spark.watermarkDelayMs")

    Duration(s"$watermark millis")
  }

  private def randomQueryName: String = randomUUID.toString.replace("-", "")
}
