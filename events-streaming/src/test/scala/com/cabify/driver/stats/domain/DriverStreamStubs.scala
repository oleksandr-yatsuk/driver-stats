package com.cabify.driver.stats.domain

import com.cabify.driver.stats.stream.{DriverRatedEvent, DriverRatedStream, DriverTippedEvent, DriverTippedStream, JourneyFinishedEvent, JourneyFinishedStream}
import org.apache.spark.sql.Dataset
import org.apache.spark.sql.execution.streaming.MemoryStream

import scala.concurrent.duration.Duration

trait DriverStreamStubs {

  protected class JourneyFinishedStreamStub(
      stream: MemoryStream[JourneyFinishedEvent],
      watermarkThreshold: Duration
  ) extends JourneyFinishedStream(null, watermarkThreshold) {
    override def events: Dataset[JourneyFinishedEvent] =
      stream.toDS
        .withWatermark(watermark.column, watermark.thresholdMillis)
  }

  protected class DriverTippedStreamStub(
      stream: MemoryStream[DriverTippedEvent],
      watermarkThreshold: Duration
  ) extends DriverTippedStream(null, watermarkThreshold) {
    override def events: Dataset[DriverTippedEvent] =
      stream.toDS
        .withWatermark(watermark.column, watermark.thresholdMillis)
  }

  protected class DriverRatedStreamStub(
      stream: MemoryStream[DriverRatedEvent],
      watermarkThreshold: Duration
  ) extends DriverRatedStream(null, watermarkThreshold) {
    override def events: Dataset[DriverRatedEvent] =
      stream.toDS
        .withWatermark(watermark.column, watermark.thresholdMillis)
  }

}
