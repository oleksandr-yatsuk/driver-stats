package com.cabify.driver.stats.domain

import cabify.input.FinishReason.DROP_OFF
import com.cabify.driver.stats.base.BaseSparkSpec
import com.cabify.driver.stats.stream.{DriverRatedEvent, DriverTippedEvent, JourneyFinishedEvent, KafkaStreamMock}
import org.apache.spark.sql.execution.streaming.MemoryStream

import scala.concurrent.duration.Duration

class DriverStatsSpec extends BaseSparkSpec with DriverStreamStubs with KafkaStreamMock {
  import spark.implicits._

  "drop offs" should "be aggregated for driver in time interval" in {
    val journeyFinishedStream = MemoryStream[JourneyFinishedEvent]
    val journeyFinished       = new JourneyFinishedStreamStub(journeyFinishedStream, Duration("0 seconds"))
    val journeyFinishedEvents = Seq(
      /*JourneyFinishedEvent(
        "driver-1",
        "journey-1",
        "rider",
        DROP_OFF.name,
        Some(instant("2023-07-04T09:23:43Z")),
        Some(instant("2023-07-04T09:24:09Z")),
        0,
        0
      ),*/
      JourneyFinishedEvent(
        "driver-1",
        "journey-2",
        "rider",
        DROP_OFF.name,
        Some(instant("2023-07-09T23:33:30Z")),
        Some(instant("2023-07-09T23:33:20Z")),
        0,
        0
      ),
      JourneyFinishedEvent(
        "driver-1",
        "journey-3",
        "rider",
        DROP_OFF.name,
        Some(instant("2023-07-09T23:33:30Z")),
        Some(instant("2023-07-09T23:33:00Z")),
        0,
        0
      )
    )
    journeyFinishedStream.addData(journeyFinishedEvents)

    val driverTipped = new DriverTippedStreamStub(MemoryStream[DriverTippedEvent], Duration("0 millis"))
    val driverRated  = new DriverRatedStreamStub(MemoryStream[DriverRatedEvent], Duration("0 millis"))

    val interval            = Duration("10 seconds")
    val tipsAllowedDelay    = Duration("0 millis")
    val ratingsAllowedDelay = Duration("0 millis")

    val driverStatistics = new DriverStatistics(
      journeyFinished,
      driverTipped,
      driverRated,
      interval,
      tipsAllowedDelay,
      ratingsAllowedDelay
    )

    val events = driverStatistics.events

    // val actual = readAllEvents(events)

    val query = events.writeStream
      .queryName("qn")
      .format("memory")
      .outputMode("append")
      .start()

    journeyFinishedStream.addData(journeyFinishedEvents)

    query.processAllAvailable()

    val actual = spark
      .sql(s"SELECT * FROM qn")
      .as[DriverStatsEvent]
      .collect()

    actual should have size 0
  }
}
