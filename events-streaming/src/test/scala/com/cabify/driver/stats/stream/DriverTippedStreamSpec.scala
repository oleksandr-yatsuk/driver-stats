package com.cabify.driver.stats.stream

import cabify.input.DriverTipped
import com.cabify.driver.stats.base.BaseSparkSpec
import com.cabify.driver.stats.kafka.KafkaEvent
import com.cabify.driver.stats.stream.DriverTippedStream.tippedAtPath
import com.google.protobuf.timestamp.Timestamp

import scala.concurrent.duration.Duration

class DriverTippedStreamSpec extends BaseSparkSpec with KafkaStreamMock {
  "kafka protobuf events" should "be converted to driver tipped events" in {
    val kafkaEvents = Seq(
      KafkaEvent(
        DriverTipped("a", "b", "1", Some(Timestamp(1623020842)), 1023).toByteArray
      ),
      KafkaEvent(
        DriverTipped("c", "d", "2", Some(Timestamp(1605134001)), 10000).toByteArray
      )
    )
    val expected = Seq(
      DriverTippedEvent("a", "b", "1", Some(instant("2021-06-06T23:07:22Z")), 1023),
      DriverTippedEvent("c", "d", "2", Some(instant("2020-11-11T22:33:21Z")), 10000)
    )
    val (kafkaStream, memoryStream) = kafkaSource
    memoryStream.addData(kafkaEvents)

    val driverRatedStream = new DriverTippedStream(kafkaStream, Duration("1 second"))

    val events = driverRatedStream.events

    val actual = readAllEvents(events)

    actual should have length expected.size
    actual should contain theSameElementsAs expected
  }

  "rated at timestamp" should "be converted to UTC local date time" in {
    val kafkaEvents = Seq(
      KafkaEvent(
        DriverTipped("a", "b", "1", Some(Timestamp(1647432476)), 4).toByteArray
      ),
      KafkaEvent(
        DriverTipped("c", "d", "2", Some(Timestamp(1612232349)), 10).toByteArray
      )
    )
    val expected = Seq(
      DriverTippedEvent("a", "b", "1", Some(instant("2022-03-16T13:07:56+01:00")), 4),
      DriverTippedEvent("c", "d", "2", Some(instant("2021-02-01T22:34:09-03:45")), 10)
    )
    val (kafkaStream, memoryStream) = kafkaSource

    val driverRatedStream = new DriverTippedStream(kafkaStream, Duration("1 second"))

    val events = driverRatedStream.events

    memoryStream.addData(kafkaEvents)

    val actual = readAllEvents(events)

    actual should have length expected.size
    actual should contain theSameElementsAs expected
  }

  "event timestamp watermark" should "be set on tippedAt field" in {
    val watermarkThreshold = Duration("3764 seconds")
    val (kafkaStream, _)   = kafkaSource

    val driverRatedStream = new DriverTippedStream(kafkaStream, watermarkThreshold)

    val events = driverRatedStream.events

    val actualWatermark = watermarkField(tippedAtPath, events)

    actualWatermark shouldBe watermarkThreshold
  }
}
