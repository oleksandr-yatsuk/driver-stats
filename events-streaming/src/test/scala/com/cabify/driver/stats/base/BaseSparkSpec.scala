package com.cabify.driver.stats.base

import org.apache.spark.sql.{SQLContext, SparkSession}
import org.apache.spark.sql.types.StructType

trait BaseSparkSpec extends BaseSpec {
  implicit lazy val spark: SparkSession = {
    val _spark = SparkSession
      .builder()
      .master("local[*]")
      .appName("Spec Spark Session")
      .config("spark.sql.caseSensitive", "true")
      .config("spark.sql.session.timeZone", "UTC")
      .config("spark.sql.shuffle.partitions", "1")
      .config("spark.broadcast.compress", "false")
      .config("spark.shuffle.compress", "false")
      .config("spark.shuffle.spill.compress", "false")
      .config("spark.sql.streaming.statefulOperator.checkCorrectness.enabled", "false")
      .config("spark.sql.streaming.unsupportedOperationCheck", "true")
      .config("spark.sql.streaming.statefulOperator.allowMultiple", "true")
      .getOrCreate()

    _spark.sparkContext.hadoopConfiguration.set("mapreduce.fileoutputcommitter.marksuccessfuljobs", "false")
    _spark.sparkContext.hadoopConfiguration.set("spark.app.id", _spark.conf.get("spark.app.id"))

    _spark
  }

  implicit lazy val sqlContext: SQLContext = spark.sqlContext

  protected def equal(a: StructType, b: StructType, allFieldsNullable: Boolean = true): Boolean =
    if (allFieldsNullable)
      allNullable(a.treeString) == allNullable(b.treeString)
    else
      a == b

  protected def allNullable(schemaTree: String): String =
    schemaTree.replaceAll("nullable = false", "nullable = true")
}
