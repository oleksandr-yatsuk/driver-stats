package com.cabify.driver.stats.base

import com.cabify.driver.stats.utils.time.TimeZones.setUtcTimeZone
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers

import java.time.format.DateTimeFormatter.ISO_ZONED_DATE_TIME
import java.time.{Instant, ZoneId, ZonedDateTime}

trait BaseSpec extends AnyFlatSpecLike with Matchers {
  setUtcTimeZone()

  protected def instant(dateTime: String): Instant =
    ZonedDateTime
      .parse(dateTime, ISO_ZONED_DATE_TIME)
      .withZoneSameInstant(ZoneId.of("UTC"))
      .toInstant
}
