package com.cabify.driver.stats.domain

case class DriverRatings(
                          override val window: DriverStatisticsInterval,
                          override val driverId: String,
                          rating: Double
) extends DriverInterval(window, driverId)
