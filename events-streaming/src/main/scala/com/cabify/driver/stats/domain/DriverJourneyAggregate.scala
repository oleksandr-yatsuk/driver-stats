package com.cabify.driver.stats.domain

import com.cabify.driver.stats.stream.FinishReason.{driverCancel, dropOff}
import com.cabify.driver.stats.stream.JourneyFinishedStream
import com.cabify.driver.stats.utils.variables.ValName.{name, path}
import org.apache.spark.sql.Encoders.product
import org.apache.spark.sql.functions.{sum, when, window => windowInterval}
import org.apache.spark.sql.{Column, Dataset, Encoder}

import scala.concurrent.duration.Duration

class DriverJourneyAggregate(
    journeyFinished: JourneyFinishedStream,
    interval: Duration
) {
  import DriverJourneyAggregate._

  private lazy val _journeys =
    journeyFinished.events
      .groupBy(windowInterval(journeyFinished.endedAt, intervalMillis), journeyFinished.driverId)
      .agg(
        sum(
          when(journeyFinished.finishReason === dropOff, 1).otherwise(0)
        ).as(driverDropOffsPath),
        sum(
          when(journeyFinished.finishReason === driverCancel, 1).otherwise(0)
        ).as(driverCancelsPath),
        sum(journeyFinished.price).as(driverTotalPricePath),
        sum(journeyFinished.distance).as(driverTotalDistancePath)
      )
      .as[DriverJourneys]

  def journeys: Dataset[DriverJourneys] = _journeys

  def driverId: Column = journeys(driverIdPath)
  def window: Column   = journeys(windowPath)

  def windowStart: Column = journeys(windowStartPath)
  def windowEnd: Column   = journeys(windowEndPath)

  private def intervalMillis: String = s"${interval.toMillis} milliseconds"
}

object DriverJourneyAggregate {
  private implicit val journeysEncoder: Encoder[DriverJourneys] = product[DriverJourneys]

  private val driverIdPath: String = name[DriverJourneys](_.driverId)

  private val windowPath: String      = name[DriverJourneys](_.window)
  private val windowStartPath: String = name[DriverJourneys](_.window.start)
  private val windowEndPath: String   = name[DriverJourneys](_.window.end)

  private val driverDropOffsPath: String      = name[DriverJourneys](_.dropOffs)
  private val driverCancelsPath: String       = name[DriverJourneys](_.driverCancels)
  private val driverTotalPricePath: String    = name[DriverJourneys](_.totalPrice)
  private val driverTotalDistancePath: String = name[DriverJourneys](_.totalDistance)
}
