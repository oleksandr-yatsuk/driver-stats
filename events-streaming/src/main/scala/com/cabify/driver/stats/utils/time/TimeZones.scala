package com.cabify.driver.stats.utils.time

import java.util.TimeZone

object TimeZones {
  private val utc: TimeZone = TimeZone.getTimeZone("UTC")

  def setUtcTimeZone(): Unit = TimeZone.setDefault(utc)
}
