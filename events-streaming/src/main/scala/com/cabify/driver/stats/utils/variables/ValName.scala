package com.cabify.driver.stats.utils.variables

import com.github.dwickern.macros.NameOfImpl

import scala.language.experimental.macros

trait ValName {
  def name(expr: Any): String = macro NameOfImpl.nameOf

  def name[T](expr: T => Any): String = macro NameOfImpl.nameOf

  def path(expr: Any): String = macro ValNameImpl.qualifiedNameOf

  def path[T](expr: T => Any): String = macro ValNameImpl.qualifiedNameOf
}
object ValName extends ValName
