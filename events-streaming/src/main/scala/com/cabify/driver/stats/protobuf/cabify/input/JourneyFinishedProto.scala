// Generated by the Scala Plugin for the Protocol Buffer Compiler.
// Do not edit!
//
// Protofile syntax: PROTO3

package cabify.input

object JourneyFinishedProto extends _root_.scalapb.GeneratedFileObject {
  lazy val dependencies: Seq[_root_.scalapb.GeneratedFileObject] = Seq(
    com.google.protobuf.timestamp.TimestampProto
  )
  lazy val messagesCompanions
      : Seq[_root_.scalapb.GeneratedMessageCompanion[_ <: _root_.scalapb.GeneratedMessage]] =
    Seq[_root_.scalapb.GeneratedMessageCompanion[_ <: _root_.scalapb.GeneratedMessage]](
      cabify.input.JourneyFinished
    )
  private lazy val ProtoBytes: _root_.scala.Array[Byte] =
    scalapb.Encoding.fromBase64(
      scala.collection.immutable
        .Seq(
          """Cilwcm90by9jYWJpZnkvaW5wdXQvam91cm5leV9maW5pc2hlZC5wcm90bxIMY2FiaWZ5LmlucHV0Gh9nb29nbGUvcHJvdG9id
  WYvdGltZXN0YW1wLnByb3RvIsYDCg9Kb3VybmV5RmluaXNoZWQSLQoKam91cm5leV9pZBgBIAEoCUIO4j8LEglqb3VybmV5SWRSC
  WpvdXJuZXlJZBIqCglkcml2ZXJfaWQYAiABKAlCDeI/ChIIZHJpdmVySWRSCGRyaXZlcklkEicKCHJpZGVyX2lkGAMgASgJQgziP
  wkSB3JpZGVySWRSB3JpZGVySWQSUgoNZmluaXNoX3JlYXNvbhgEIAEoDjIaLmNhYmlmeS5pbnB1dC5GaW5pc2hSZWFzb25CEeI/D
  hIMZmluaXNoUmVhc29uUgxmaW5pc2hSZWFzb24SSQoKc3RhcnRlZF9hdBgFIAEoCzIaLmdvb2dsZS5wcm90b2J1Zi5UaW1lc3Rhb
  XBCDuI/CxIJc3RhcnRlZEF0UglzdGFydGVkQXQSQwoIZW5kZWRfYXQYBiABKAsyGi5nb29nbGUucHJvdG9idWYuVGltZXN0YW1wQ
  gziPwkSB2VuZGVkQXRSB2VuZGVkQXQSKQoIZGlzdGFuY2UYByABKA1CDeI/ChIIZGlzdGFuY2VSCGRpc3RhbmNlEiAKBXByaWNlG
  AggASgNQgriPwcSBXByaWNlUgVwcmljZSqcAQoMRmluaXNoUmVhc29uEhsKCERST1BfT0ZGEAAaDeI/ChIIRFJPUF9PRkYSIwoMU
  klERVJfQ0FOQ0VMEAEaEeI/DhIMUklERVJfQ0FOQ0VMEiUKDURSSVZFUl9DQU5DRUwQAhoS4j8PEg1EUklWRVJfQ0FOQ0VMEiMKD
  EFETUlOX0NBTkNFTBADGhHiPw4SDEFETUlOX0NBTkNFTErnCgoGEgQAABcBCggKAQwSAwAAEgoICgECEgMBABUKCQoCAwASAwMAK
  QpiCgIEABIEBwAQARpWIEEgam91cm5leSB0aGF0IGhhcyBlbmRlZCBmb3IgZGlmZmVyZW50IHJlYXNvbnMKIE1RVFQgdG9waWM6I
  CJpbnB1dC9qb3VybmV5X2ZpbmlzaGVkIgoKCgoDBAABEgMHCBcKLwoEBAACABIDCAIrIiIgVW5pcXVlIGlkZW50aWZpZXIgb2Ygd
  GhlIGpvdXJuZXkKCgwKBQQAAgAFEgMIAggKDAoFBAACAAESAwgJEwoMCgUEAAIAAxIDCCkqCi4KBAQAAgESAwkCKyIhIFVuaXF1Z
  SBpZGVudGlmaWVyIG9mIHRoZSBkcml2ZXIKCgwKBQQAAgEFEgMJAggKDAoFBAACAQESAwkJEgoMCgUEAAIBAxIDCSkqCkUKBAQAA
  gISAwoCKyI4IFVuaXF1ZSBpZGVudGlmaWVyIG9mIHRoZSByaWRlciAodGhlIHBlcnNvbiB0cmF2ZWxsaW5nKQoKDAoFBAACAgUSA
  woCCAoMCgUEAAICARIDCgkRCgwKBQQAAgIDEgMKKSoKLwoEBAACAxIDCwIrIiIgVGhlIHJlYXNvbiB3aHkgdGhlIGpvdXJuZXkgZ
  W5kZWQKCgwKBQQAAgMGEgMLAg4KDAoFBAACAwESAwsPHAoMCgUEAAIDAxIDCykqCiwKBAQAAgQSAwwCKyIfIFRoZSBzdGFydCB0a
  W1lIG9mIHRoZSBqb3VybmV5CgoMCgUEAAIEBhIDDAIbCgwKBQQAAgQBEgMMHCYKDAoFBAACBAMSAwwpKgoqCgQEAAIFEgMNAisiH
  SBUaGUgZW5kIHRpbWUgb2YgdGhlIGpvdXJuZXkKCgwKBQQAAgUGEgMNAhsKDAoFBAACBQESAw0cJAoMCgUEAAIFAxIDDSkqCmoKB
  AQAAgYSAw4CKyJdIFRvdGFsIGRpc3RhbmNlIG9mIHRoZSBqb3VybmV5IGluIG1ldGVycy4gT25seSBjb250YWlucyB2YWx1ZSB3a
  GVuIGZpbmlzaF9yZWFzb24gPT0gRFJPUF9PRkYKCgwKBQQAAgYFEgMOAggKDAoFBAACBgESAw4JEQoMCgUEAAIGAxIDDikqCmwKB
  AQAAgcSAw8CKyJfIFRoZSBwcmljZSBvZiB0aGUgam91cm5leSBpbiBjZW50cyBvZiBldXJvLiBPbmx5IGNvbnRhaW5zIHZhbHVlI
  HdoZW4gZmluaXNoX3JlYXNvbiA9PSBEUk9QX09GRgoKDAoFBAACBwUSAw8CCAoMCgUEAAIHARIDDwkOCgwKBQQAAgcDEgMPKSoKC
  goCBQASBBIAFwEKCgoDBQABEgMSBREKQQoEBQACABIDEwIUIjQgVGhlIGpvdXJuZXkgZW5kZWQgd2l0aCB0aGUgcmlkZXIgYmVpb
  mcgZHJvcHBlZCBvZmYKCgwKBQUAAgABEgMTAgoKDAoFBQACAAISAxMSEwo1CgQFAAIBEgMUAhQiKCBUaGUgam91cm5leSB3YXMgY
  2FuY2VsbGVkIGJ5IHRoZSByaWRlcgoKDAoFBQACAQESAxQCDgoMCgUFAAIBAhIDFBITCjYKBAUAAgISAxUCFCIpIFRoZSBqb3Vyb
  mV5IHdhcyBjYW5jZWxsZWQgYnkgdGhlIGRyaXZlcgoKDAoFBQACAgESAxUCDwoMCgUFAAICAhIDFRITCjcKBAUAAgMSAxYCFCIqI
  FRoZSBqb3VybmV5IHdhcyBjYW5jZWxsZWQgYnkgYW4gb3BlcmF0b3IKCgwKBQUAAgMBEgMWAg4KDAoFBQACAwISAxYSE2IGcHJvd
  G8z"""
        )
        .mkString
    )
  lazy val scalaDescriptor: _root_.scalapb.descriptors.FileDescriptor = {
    val scalaProto = com.google.protobuf.descriptor.FileDescriptorProto.parseFrom(ProtoBytes)
    _root_.scalapb.descriptors.FileDescriptor
      .buildFrom(scalaProto, dependencies.map(_.scalaDescriptor))
  }
  lazy val javaDescriptor: com.google.protobuf.Descriptors.FileDescriptor = {
    val javaProto = com.google.protobuf.DescriptorProtos.FileDescriptorProto.parseFrom(ProtoBytes)
    com.google.protobuf.Descriptors.FileDescriptor.buildFrom(
      javaProto,
      _root_.scala.Array(
        com.google.protobuf.timestamp.TimestampProto.javaDescriptor
      )
    )
  }
  @deprecated(
    "Use javaDescriptor instead. In a future version this will refer to scalaDescriptor.",
    "ScalaPB 0.5.47"
  )
  def descriptor: com.google.protobuf.Descriptors.FileDescriptor = javaDescriptor
}
