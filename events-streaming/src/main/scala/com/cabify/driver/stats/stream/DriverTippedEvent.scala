package com.cabify.driver.stats.stream

import java.time.Instant

case class DriverTippedEvent(
    driverId: String,
    riderId: String,
    journeyId: String,
    tippedAt: Option[Instant],
    amount: Int
)
