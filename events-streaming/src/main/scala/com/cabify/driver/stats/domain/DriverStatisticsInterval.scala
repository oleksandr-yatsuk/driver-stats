package com.cabify.driver.stats.domain

import java.time.Instant

case class DriverStatisticsInterval(start: Instant, end: Instant)
