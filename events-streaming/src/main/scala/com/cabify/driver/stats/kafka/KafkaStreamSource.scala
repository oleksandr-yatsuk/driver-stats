package com.cabify.driver.stats.kafka

import org.apache.spark.sql.Encoders.product
import org.apache.spark.sql.{Dataset, Encoder, SparkSession}

case class KafkaStreamSource(
    kafka: KafkaServerConf,
    topic: KafkaTopicConf
)(implicit spark: SparkSession) {
  import KafkaStreamSource._

  def stream: Dataset[KafkaEvent] =
    spark.readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", kafka.uri)
      .option("subscribe", topic.topic)
      .option("includeHeaders", topic.includeHeaders)
      .option("startingOffsets", topic.startingOffsets)
      .option("failOnDataLoss", topic.failOnDataLoss)
      .load()
      .as[KafkaEvent]
}

object KafkaStreamSource {
  private implicit val eventEncoder: Encoder[KafkaEvent] = product[KafkaEvent]
}
