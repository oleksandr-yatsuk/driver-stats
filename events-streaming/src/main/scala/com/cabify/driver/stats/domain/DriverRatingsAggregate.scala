package com.cabify.driver.stats.domain

import com.cabify.driver.stats.stream.DriverRatedStream
import com.cabify.driver.stats.utils.variables.ValName.{name, path}
import org.apache.spark.sql.Encoders.product
import org.apache.spark.sql.functions.{avg, window => windowInterval}
import org.apache.spark.sql.{Column, Dataset, Encoder}

import scala.concurrent.duration.Duration

class DriverRatingsAggregate(driverRated: DriverRatedStream, interval: Duration) {
  import DriverRatingsAggregate._

  private val _ratings =
    driverRated.events
      .groupBy(windowInterval(driverRated.ratedAt, intervalMillis(interval)), driverRated.driverId)
      .agg(
        avg(driverRated.rating).as(driverRatingPath)
      )
      .as[DriverRatings]

  def ratings: Dataset[DriverRatings] = _ratings

  def driverId: Column = ratings(driverIdPath)
  def window: Column   = ratings(windowPath)
}

object DriverRatingsAggregate {
  private implicit val driverRatingsEncoder: Encoder[DriverRatings] = product[DriverRatings]

  private val driverIdPath: String = name[DriverRatings](_.driverId)

  private val windowPath: String      = name[DriverRatings](_.window)
  private val windowStartPath: String = path[DriverRatings](_.window.start)
  private val windowEndPath: String   = path[DriverRatings](_.window.end)

  private val driverRatingPath: String = name[DriverRatings](_.rating)
}
