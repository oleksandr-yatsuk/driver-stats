package com.cabify.driver.stats.stream

import cabify.output.DriverStats
import com.cabify.driver.stats.domain.DriverStatsEvent
import com.cabify.driver.stats.kafka.{KafkaSinkEvent, KafkaStreamSink}
import com.cabify.driver.stats.protobuf.InstantAs
import org.apache.spark.sql.streaming.StreamingQuery
import org.apache.spark.sql.{Dataset, Encoder, Encoders}

class DriverStatsStream(kafkaSink: KafkaStreamSink) {
  import DriverStatsStream._

  def write(stats: Dataset[DriverStatsEvent], mode: String): StreamingQuery =
    kafkaSink.write(
      stats.map { stats =>
        KafkaSinkEvent(
          DriverStats(
            stats.driverId,
            Option(stats.intervalStart.ts),
            Option(stats.intervalEnd.ts),
            stats.totalDropOffs.toInt,
            stats.totalDriverCancels.toInt,
            stats.totalMetersDistance.toInt,
            stats.totalCentsPrice.toInt,
            stats.totalCentsTips.toInt,
            stats.averageRating.toFloat
          )
        )
      },
      mode
    )
}

object DriverStatsStream {
  private implicit val kafkaDataEncoder: Encoder[KafkaSinkEvent] = Encoders.product[KafkaSinkEvent]
}
