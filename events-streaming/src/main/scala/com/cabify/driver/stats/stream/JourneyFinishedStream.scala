package com.cabify.driver.stats.stream

import cabify.input.JourneyFinished
import com.cabify.driver.stats.kafka.KafkaStreamSource
import com.cabify.driver.stats.protobuf.ProtobufTimestampAs
import com.cabify.driver.stats.utils.variables.ValName.name
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.{Column, Dataset, Encoder, Encoders}

import scala.concurrent.duration.Duration

class JourneyFinishedStream(
    kafkaSource: KafkaStreamSource,
    watermarkThreshold: Duration
) extends Watermarked {
  import JourneyFinishedStream._
  import scalapb.spark.Implicits._

  private lazy val _events = kafkaSource.stream
    .map(_.value)
    .map(JourneyFinished.parseFrom)
    .map { proto =>
      JourneyFinishedEvent(
        proto.journeyId,
        proto.driverId,
        proto.riderId,
        proto.finishReason.name,
        proto.startedAt.map(_.instant),
        proto.endedAt.map(_.instant),
        proto.distance,
        proto.price
      )
    }
    .withWatermark(watermark.column, watermark.thresholdMillis)

  def watermark: Watermark = Watermark(endedAtPath, watermarkThreshold)

  def events: Dataset[JourneyFinishedEvent] = _events

  def driverId: Column     = events(driverIdPath)
  def journeyId: Column    = events(journeyIdPath)
  def endedAt: Column      = events(endedAtPath)
  def distance: Column     = events(distancePath)
  def price: Column        = events(pricePath)
  def finishReason: Column = events(finishReasonPath)
}

object JourneyFinishedStream {
  private implicit val encoder: Encoder[JourneyFinishedEvent] = Encoders.product[JourneyFinishedEvent]

  private val driverIdPath        = name[DriverRatedEvent](_.driverId)
  private val journeyIdPath       = name[DriverRatedEvent](_.journeyId)
  private val distancePath        = name[JourneyFinishedEvent](_.distance)
  private val pricePath           = name[JourneyFinishedEvent](_.price)
  private val finishReasonPath    = name[JourneyFinishedEvent](_.finishReason)
  private[stream] val endedAtPath = name[JourneyFinishedEvent](_.endedAt)

  val endedAtColumn: Column      = col(endedAtPath)
  val finishReasonColumn: Column = col(finishReasonPath)
  val distanceColumn: Column     = col(distancePath)
  val priceColumn: Column        = col(pricePath)
}
