package com.cabify.driver.stats.domain

import java.time.Instant

case class DriverStatsEvent(
    driverId: String,
    window: DriverStatisticsInterval,
    totalDropOffs: Long = 0,
    totalDriverCancels: Long = 0,
    totalMetersDistance: Long = 0,
    totalCentsPrice: Long = 0,
    totalCentsTips: Long = 0,
    averageRating: Double = 0.0f
) {
  def intervalStart: Instant = window.start
  def intervalEnd: Instant   = window.end
}
