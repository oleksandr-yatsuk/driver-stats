package com.cabify.driver.stats.stream

import java.time.Instant

case class JourneyFinishedEvent(
    journeyId: String,
    driverId: String,
    riderId: String,
    finishReason: String,
    startedAt: Option[Instant],
    endedAt: Option[Instant],
    distance: Int,
    price: Int
)
