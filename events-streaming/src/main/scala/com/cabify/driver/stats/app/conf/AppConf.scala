package com.cabify.driver.stats.app.conf

import com.cabify.driver.stats.kafka.{KafkaServerConf, KafkaSinkConf, KafkaTopicConf}
import scopt.OptionParser

import scala.concurrent.duration.{Duration, SECONDS}

case class AppConf(
    kafka: KafkaServerConf,
    input: InputConf,
    driverTipsAllowedDelay: Duration,
    driverRatingAllowedDelay: Duration,
    driverStatsInterval: Duration,
    driverStatsTopic: String,
    checkpointLocation: String
) {
  def sinkConf: KafkaSinkConf =
    KafkaSinkConf(kafka, driverStatsTopic, checkpointLocation)
}

object AppConf {
  private val defaultDriverStatsInterval = Duration("10 minutes")

  val default: AppConf = AppConf(
    KafkaServerConf("localhost", 9092),
    InputConf(KafkaTopicConf("journey_finished"), KafkaTopicConf("driver_rated"), KafkaTopicConf("driver_tipped")),
    Duration("1 minute"),
    Duration("1 minute"),
    defaultDriverStatsInterval,
    "driver_stats",
    ".kafka"
  )

  def parse(args: Seq[String], app: String, failOnUnknownArg: Boolean = false): Option[AppConf] = {
    val parser = new OptionParser[AppConf](app) {
      override def errorOnUnknownArgument: Boolean   = failOnUnknownArg
      override def showUsageOnError: Option[Boolean] = Some(true)
    }

    parseKafkaServerConf(parser)
    parseInputConf(parser)
    parseAllowedDelays(parser)
    parseDriverStatsInterval(parser)
    parseDriveStatsTopic(parser)
    parseCheckpointLocation(parser)

    parser.parse(args, default)
  }

  private def parseKafkaServerConf(parser: OptionParser[AppConf]): Unit = {
    parser
      .opt[String]("kafka-host")
      .required()
      .action((host, conf) =>
        conf.copy(
          kafka = conf.kafka.copy(host = host)
        )
      )
      .text("Kafka host")

    parser
      .opt[Int]("kafka-port")
      .required()
      .action((port, conf) =>
        conf.copy(
          kafka = conf.kafka.copy(port = port)
        )
      )
      .text("Kafka port")
  }

  private def parseInputConf(parser: OptionParser[AppConf]): Unit = {
    parser
      .opt[String]("journey-finished-topic")
      .required()
      .action((journeyFinished, conf) =>
        conf.copy(
          input = conf.input.copy(journeyFinished =
            conf.input.journeyFinished.copy(
              topic = journeyFinished
            )
          )
        )
      )
      .text("Journey finished Kafka topic")

    parser
      .opt[String]("driver-rated-topic")
      .required()
      .action((driverRated, conf) =>
        conf.copy(
          input = conf.input.copy(driverRated =
            conf.input.driverRated.copy(
              topic = driverRated
            )
          )
        )
      )
      .text("Driver rated Kafka topic")

    parser
      .opt[String]("driver-tipped-topic")
      .required()
      .action((driverTipped, conf) =>
        conf.copy(
          input = conf.input.copy(driverTipped =
            conf.input.driverTipped.copy(
              topic = driverTipped
            )
          )
        )
      )
      .text("Driver tipped Kafka topic")
  }

  private def parseAllowedDelays(parser: OptionParser[AppConf]): Unit = {
    parser
      .opt[Int]("driver-tips-allowed-delay-seconds")
      .required()
      .action((interval, conf) =>
        conf.copy(
          driverTipsAllowedDelay = Duration(interval, SECONDS)
        )
      )
      .text("Driver tips allowed time delay after journey is finished")

    parser
      .opt[Int]("driver-rating-allowed-delay-seconds")
      .required()
      .action((interval, conf) =>
        conf.copy(
          driverRatingAllowedDelay = Duration(interval, SECONDS)
        )
      )
      .text("Driver rating allowed time delay after journey is finished")
  }

  private def parseDriverStatsInterval(parser: OptionParser[AppConf]): Unit =
    parser
      .opt[Int]("driver-stats-interval-seconds")
      .optional()
      .action((interval, conf) =>
        conf.copy(
          driverStatsInterval = Duration(interval, SECONDS)
        )
      )
      .text("Driver stats time interval")

  private def parseDriveStatsTopic(parser: OptionParser[AppConf]): Unit =
    parser
      .opt[String]("driver-stats-topic")
      .required()
      .action((driverStats, conf) =>
        conf.copy(
          driverStatsTopic = driverStats
        )
      )
      .text("Driver stats Kafka topic")

  private def parseCheckpointLocation(parser: OptionParser[AppConf]): Unit =
    parser
      .opt[String]("checkpoint-location")
      .required()
      .action((location, conf) =>
        conf.copy(
          checkpointLocation = location
        )
      )
      .text("Kafka checkpoint location")
}
