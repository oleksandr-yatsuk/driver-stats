package com.cabify.driver.stats.kafka

import java.sql.Timestamp

case class KafkaEvent(
    value: Array[Byte],
    key: Option[Array[Byte]] = None,
    topic: Option[String] = None,
    partition: Option[Int] = None,
    offset: Option[Long] = None,
    timestamp: Option[Timestamp] = None,
    timestampType: Option[Int] = None,
    headers: Option[Seq[Header]] = None
)

case class Header(key: String, value: Array[Byte])
