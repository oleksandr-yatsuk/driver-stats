package com.cabify.driver.stats

import com.google.protobuf.timestamp.Timestamp

import java.time.Instant
import java.time.Instant.ofEpochSecond

package object protobuf {
  implicit class ProtobufTimestampAs(ts: Timestamp) {
    def instant: Instant = ofEpochSecond(ts.seconds, ts.nanos)
  }

  implicit class InstantAs(dateTime: Instant) {
    def ts: Timestamp = {
      val millis = dateTime.toEpochMilli

      Timestamp(millis / 1000, (millis % 1000).toInt)
    }
  }
}
