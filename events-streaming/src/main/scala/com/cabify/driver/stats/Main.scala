package com.cabify.driver.stats

import com.cabify.driver.stats.app.conf.AppConf
import com.cabify.driver.stats.app.{StatisticsApp, sparkSession}

object Main {
  val appName = "Driver statistics"

  def main(args: Array[String]): Unit = {
    AppConf.parse(args, appName) match {
      case Some(conf) =>
        val spark = sparkSession(appName)
        val app   = new StatisticsApp(conf)(spark)

        app.run()
      case None => println("Failed to parse the arguments")
    }
  }
}
