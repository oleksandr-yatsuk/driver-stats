package com.cabify.driver.stats.stream

import cabify.input.DriverTipped
import com.cabify.driver.stats.kafka.KafkaStreamSource
import com.cabify.driver.stats.protobuf.ProtobufTimestampAs
import com.cabify.driver.stats.utils.variables.ValName.name
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.{Column, Dataset, Encoder, Encoders}

import scala.concurrent.duration.Duration

class DriverTippedStream(
    kafkaSource: KafkaStreamSource,
    watermarkThreshold: Duration
) extends Watermarked {
  import DriverTippedStream._
  import scalapb.spark.Implicits._

  private lazy val _events: Dataset[DriverTippedEvent] = kafkaSource.stream
    .map(_.value)
    .map(DriverTipped.parseFrom)
    .map { proto =>
      DriverTippedEvent(
        proto.driverId,
        proto.riderId,
        proto.journeyId,
        proto.tippedAt.map(_.instant),
        proto.amount
      )
    }
    .withWatermark(watermark.column, watermark.thresholdMillis)

  def watermark: Watermark = Watermark(tippedAtPath, watermarkThreshold)

  def events: Dataset[DriverTippedEvent] = _events

  def driverId: Column  = events(driverIdPath)
  def journeyId: Column = events(journeyIdPath)
  def amount: Column    = events(amountPath)
  def tippedAt: Column  = events(tippedAtPath)
}

object DriverTippedStream {
  private implicit val encoder: Encoder[DriverTippedEvent] = Encoders.product[DriverTippedEvent]

  private val driverIdPath         = name[DriverTippedEvent](_.driverId)
  private val journeyIdPath        = name[DriverTippedEvent](_.journeyId)
  private val amountPath           = name[DriverTippedEvent](_.amount)
  private[stream] val tippedAtPath = name[DriverTippedEvent](_.tippedAt)

  val amountColumn: Column = col(amountPath)
}
