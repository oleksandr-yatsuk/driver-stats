package com.cabify.driver.stats.utils.time

import java.time.ZoneId

object LocalDates {
  val utc: ZoneId = ZoneId.of("UTC")
}
