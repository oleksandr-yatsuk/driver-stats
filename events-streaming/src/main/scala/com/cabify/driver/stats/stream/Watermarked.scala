package com.cabify.driver.stats.stream

trait Watermarked {
  def watermark: Watermark
}
