package com.cabify.driver.stats.stream

import cabify.input.DriverRated
import com.cabify.driver.stats.kafka.KafkaStreamSource
import com.cabify.driver.stats.protobuf.ProtobufTimestampAs
import com.cabify.driver.stats.utils.variables.ValName.name
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.{Column, Dataset, Encoder, Encoders}

import scala.concurrent.duration.Duration

class DriverRatedStream(
    source: KafkaStreamSource,
    watermarkThreshold: Duration
) extends Watermarked {
  import DriverRatedStream._
  import scalapb.spark.Implicits._

  private lazy val _events = source.stream
    .map(_.value)
    .map(DriverRated.parseFrom)
    .map { proto =>
      DriverRatedEvent(
        proto.driverId,
        proto.riderId,
        proto.journeyId,
        proto.ratedAt.map(_.instant),
        proto.rating
      )
    }
    .withWatermark(watermark.column, watermark.thresholdMillis)

  def watermark: Watermark = Watermark(ratedAtPath, watermarkThreshold)

  def events: Dataset[DriverRatedEvent] = _events

  def driverId: Column  = events(driverIdPath)
  def journeyId: Column = events(journeyIdPath)
  def rating: Column    = events(ratingPath)
  def ratedAt: Column   = events(ratedAtPath)
}

object DriverRatedStream {
  private implicit val encoder: Encoder[DriverRatedEvent] = Encoders.product[DriverRatedEvent]

  private val driverIdPath        = name[DriverRatedEvent](_.driverId)
  private val journeyIdPath       = name[DriverRatedEvent](_.journeyId)
  private val ratingPath          = name[DriverRatedEvent](_.rating)
  private[stream] val ratedAtPath = name[DriverRatedEvent](_.ratedAt)

  val ratingColumn: Column = col(ratingPath)
}
