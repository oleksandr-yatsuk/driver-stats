package com.cabify.driver.stats.domain

case class DriverTips(
                       override val window: DriverStatisticsInterval,
                       override val driverId: String,
                       tips: Long
) extends DriverInterval(window, driverId)
