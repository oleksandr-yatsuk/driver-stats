package com.cabify.driver.stats.stream

import java.time.Instant

case class DriverRatedEvent(
    driverId: String,
    riderId: String,
    journeyId: String,
    ratedAt: Option[Instant],
    rating: Int
)
