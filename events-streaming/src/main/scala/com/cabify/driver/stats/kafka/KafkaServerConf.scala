package com.cabify.driver.stats.kafka

case class KafkaServerConf(host: String, port: Int) {
  def uri: String = s"$host:$port"
}
