package com.cabify.driver.stats.app

import com.cabify.driver.stats.app.conf.AppConf
import com.cabify.driver.stats.domain.DriverStatistics
import com.cabify.driver.stats.kafka.{KafkaStreamSink, KafkaStreamSource}
import com.cabify.driver.stats.stream.{DriverRatedStream, DriverStatsStream, DriverTippedStream, JourneyFinishedStream}
import com.cabify.driver.stats.utils.time.TimeZones.setUtcTimeZone
import org.apache.spark.sql.SparkSession

import scala.concurrent.duration.Duration

class StatisticsApp(conf: AppConf)(implicit spark: SparkSession) {
  setUtcTimeZone()

  def run(): Unit = {
    val watermark = Duration("1 minute")

    val journeyFinished = new JourneyFinishedStream(
      KafkaStreamSource(conf.kafka, conf.input.journeyFinished),
      watermark
    )
    val driverRated = new DriverRatedStream(
      KafkaStreamSource(conf.kafka, conf.input.driverRated),
      watermark
    )
    val driverTipped = new DriverTippedStream(
      KafkaStreamSource(conf.kafka, conf.input.driverTipped),
      watermark
    )

    val driverStats = new DriverStatistics(
      journeyFinished,
      driverTipped,
      driverRated,
      conf.driverStatsInterval,
      conf.driverTipsAllowedDelay,
      conf.driverRatingAllowedDelay
    )

    val sink = new DriverStatsStream(
      new KafkaStreamSink(conf.sinkConf)
    )
    sink
      .write(driverStats.events, "append")
      .awaitTermination()
  }
}
