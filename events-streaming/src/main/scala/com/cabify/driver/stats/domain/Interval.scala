package com.cabify.driver.stats.domain

import com.cabify.driver.stats.domain.Interval.expression
import org.apache.spark.sql.Column
import org.apache.spark.sql.functions.expr

import scala.concurrent.duration.Duration

case class Interval(duration: Duration) extends Column(expression(duration))

object Interval {
  private def expression(duration: Duration) = {
    val millis         = duration.toMillis
    val intervalColumn = expr(s"INTERVAL $millis milliseconds")

    intervalColumn.expr
  }
}
