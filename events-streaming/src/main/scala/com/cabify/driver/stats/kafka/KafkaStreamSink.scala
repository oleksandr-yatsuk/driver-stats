package com.cabify.driver.stats.kafka

import org.apache.spark.sql.Dataset
import org.apache.spark.sql.streaming.StreamingQuery

class KafkaStreamSink(sink: KafkaSinkConf) {
  def write(data: Dataset[KafkaSinkEvent], mode: String): StreamingQuery =
    data.writeStream
      .format("kafka")
      .option("kafka.bootstrap.servers", sink.kafka.uri)
      .option("topic", sink.topic)
      .option("checkpointLocation", sink.checkpointLocation)
      .outputMode(mode)
      .start()
}
