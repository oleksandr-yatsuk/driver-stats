package com.cabify.driver.stats.app.conf

import com.cabify.driver.stats.kafka.KafkaTopicConf

case class InputConf(
    journeyFinished: KafkaTopicConf,
    driverRated: KafkaTopicConf,
    driverTipped: KafkaTopicConf
)
