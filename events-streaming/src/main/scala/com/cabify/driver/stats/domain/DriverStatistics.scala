package com.cabify.driver.stats.domain

import com.cabify.driver.stats.stream.DriverRatedStream.ratingColumn
import com.cabify.driver.stats.stream.DriverTippedStream.amountColumn
import com.cabify.driver.stats.stream.FinishReason.{driverCancel, dropOff}
import com.cabify.driver.stats.stream.JourneyFinishedStream.{distanceColumn, endedAtColumn, finishReasonColumn, priceColumn}
import com.cabify.driver.stats.stream.{DriverRatedStream, DriverTippedStream, JourneyFinishedStream}
import com.cabify.driver.stats.utils.variables.ValName.path
import org.apache.spark.sql.Encoders.product
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{Dataset, Encoder}

import scala.concurrent.duration.Duration

class DriverStatistics(
    journeyFinished: JourneyFinishedStream,
    driverTipped: DriverTippedStream,
    driverRated: DriverRatedStream,
    interval: Duration,
    tipsAllowedDelay: Duration,
    ratingsAllowedDelay: Duration
) {
  import DriverStatistics._

  private val tipsInterval    = Interval(tipsAllowedDelay)
  private val ratingsInterval = Interval(ratingsAllowedDelay)

  private lazy val _events = {
    val journeyEvents = journeyFinished.events
      .join(
        driverTipped.events,
        journeyFinished.driverId === driverTipped.driverId and
          journeyFinished.journeyId === driverTipped.journeyId and
          driverTipped.tippedAt.between(journeyFinished.endedAt, journeyFinished.endedAt + tipsInterval),
        "left"
      )
      .join(
        driverRated.events,
        journeyFinished.driverId === driverRated.driverId and
          journeyFinished.journeyId === driverRated.journeyId and
          driverRated.ratedAt.between(journeyFinished.endedAt, journeyFinished.endedAt + ratingsInterval),
        "left"
      )

    val statistics = journeyEvents
      .groupBy(journeyFinished.driverId, window(endedAtColumn, windowDuration))
      .agg(
        sum(
          when(finishReasonColumn === dropOff, 1).otherwise(0)
        ).as(totalDropOffs),
        sum(
          when(finishReasonColumn === driverCancel, 1).otherwise(0)
        ).as(totalDriverCancels),
        sum(priceColumn).as(totalPrice),
        sum(distanceColumn).as(totalDistance),
        sum(coalesce(amountColumn, lit(0))).as(totalTips),
        avg(coalesce(ratingColumn, lit(0))).as(averageRating)
      )

    statistics.as[DriverStatsEvent]
  }

  def events: Dataset[DriverStatsEvent] = _events

  private def windowDuration: String = s"${interval.toMillis} milliseconds"
}

object DriverStatistics {
  private implicit val driverStatsEventEncoder: Encoder[DriverStatsEvent] = product[DriverStatsEvent]

  private val totalDropOffs      = path[DriverStatsEvent](_.totalDropOffs)
  private val totalDriverCancels = path[DriverStatsEvent](_.totalDriverCancels)
  private val totalPrice         = path[DriverStatsEvent](_.totalCentsPrice)
  private val totalDistance      = path[DriverStatsEvent](_.totalMetersDistance)
  private val totalTips          = path[DriverStatsEvent](_.totalCentsTips)
  private val averageRating      = path[DriverStatsEvent](_.averageRating)
}
