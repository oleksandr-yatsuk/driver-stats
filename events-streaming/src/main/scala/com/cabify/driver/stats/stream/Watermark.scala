package com.cabify.driver.stats.stream

import scala.concurrent.duration.Duration

case class Watermark(column: String, threshold: Duration) {
  def thresholdMillis: String  = s"${threshold.toMillis} milliseconds"
}
