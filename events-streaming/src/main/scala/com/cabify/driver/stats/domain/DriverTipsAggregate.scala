package com.cabify.driver.stats.domain

import com.cabify.driver.stats.stream.DriverTippedStream
import com.cabify.driver.stats.utils.variables.ValName.{name, path}
import org.apache.spark.sql.Encoders.product
import org.apache.spark.sql.functions.{sum, window => windowInterval}
import org.apache.spark.sql.{Column, Dataset, Encoder}

import scala.concurrent.duration.Duration

class DriverTipsAggregate(driverTipped: DriverTippedStream, interval: Duration) {
  import DriverTipsAggregate._

  private val _tips =
    driverTipped.events
      .groupBy(windowInterval(driverTipped.tippedAt, intervalMillis(interval)), driverTipped.driverId)
      .agg(
        sum(driverTipped.amount).as(driverTipsPath)
      )
      .as[DriverTips]

  def tips: Dataset[DriverTips] = _tips

  def driverId: Column = tips(driverIdPath)
  def window: Column   = tips(windowPath)
}

object DriverTipsAggregate {
  private implicit val tipsEncoder: Encoder[DriverTips] = product[DriverTips]

  private val driverIdPath: String = name[DriverTips](_.driverId)

  private val windowPath: String      = name[DriverTips](_.window)
  private val windowStartPath: String = path[DriverTips](_.window.start)
  private val windowEndPath: String   = path[DriverTips](_.window.end)

  private val driverTipsPath: String = name[DriverTips](_.tips)
}
