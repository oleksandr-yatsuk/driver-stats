package com.cabify.driver.stats.utils.variables

import scala.annotation.tailrec
import scala.language.experimental.macros
import scala.reflect.macros.blackbox

object ValNameImpl {
  def qualifiedNameOf(c: blackbox.Context)(expr: c.Expr[Any]): c.Expr[String] = {
    import c.universe._

    def extractNames(tree: c.Tree): List[c.Name] = {
      tree.children.headOption match {
        case Some(child) =>
          extractNames(child) :+ tree.symbol.name
        case None =>
          List(tree.symbol.name)
      }
    }

    @tailrec
    def extract(tree: c.Tree): List[c.Name] = tree match {
      case Ident(n)           => List(n)
      case Select(tree, n)    => extractNames(tree) :+ n
      case Function(_, body)  => extract(body)
      case Block(_, expr)     => extract(expr)
      case Apply(func, _)     => extract(func)
      case TypeApply(func, _) => extract(func)
      case _                  => c.abort(c.enclosingPosition, s"Unsupported expression: $expr")
    }

    val name = extract(expr.tree)
      .drop(1)
      .mkString(".")
    reify {
      c.Expr[String] {
        Literal(Constant(name))
      }.splice
    }
  }
}
