package com.cabify.driver.stats.kafka

case class KafkaSinkConf(
    kafka: KafkaServerConf,
    topic: String,
    checkpointLocation: String
)
