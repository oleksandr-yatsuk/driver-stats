package com.cabify.driver.stats.kafka

case class KafkaTopicConf(
    topic: String,
    includeHeaders: Boolean = true,
    startingOffsets: String = "earliest",
    failOnDataLoss: Boolean = false
)
