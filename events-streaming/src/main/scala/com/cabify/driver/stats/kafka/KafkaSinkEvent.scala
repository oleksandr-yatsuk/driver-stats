package com.cabify.driver.stats.kafka

import scalapb.GeneratedMessage

case class KafkaSinkEvent(
    value: Array[Byte],
    key: Option[String] = None,
    headers: Option[Array[Header]] = None,
    topic: Option[String] = None,
    partition: Option[Int] = None
)

object KafkaSinkEvent {
  def apply(message: GeneratedMessage): KafkaSinkEvent = KafkaSinkEvent(message.toByteArray)
}
