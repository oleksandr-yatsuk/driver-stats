package com.cabify.driver.stats.domain

import com.cabify.driver.stats.stream.{DriverRatedEvent, DriverTippedEvent, JourneyFinishedEvent}
import com.cabify.driver.stats.utils.variables.ValName.name
import org.apache.spark.sql.Column
import org.apache.spark.sql.functions.col

import java.time.Instant

case class DriverJourneys(
                           override val window: DriverStatisticsInterval,
                           override val driverId: String,
                           dropOffs: Long,
                           driverCancels: Long,
                           totalPrice: Long,
                           totalDistance: Long
) extends DriverInterval(window, driverId)

case class DriverJourney(
    driverId: String,
    journeyId: String,
    finishReason: String,
    endedAt: Option[Instant],
    distance: Int,
    price: Int,
    tippedAmount: Option[Int],
    rating: Option[Int]
)

object DriverJourney {
  private val driverIdPath = name[DriverJourney](_.driverId)
  val journeyEndedAtPath   = name[DriverJourney](_.endedAt)

  val driverId: Column       = col(driverIdPath)
  val journeyEndedAt: Column = col(journeyEndedAtPath)

  def apply(journey: JourneyFinishedEvent, tip: Option[DriverTippedEvent], rating: Option[DriverRatedEvent]): DriverJourney =
    DriverJourney(
      journey.driverId,
      journey.journeyId,
      journey.finishReason,
      journey.endedAt,
      journey.distance,
      journey.price,
      tip.map(_.amount),
      rating.map(_.rating)
    )
}
