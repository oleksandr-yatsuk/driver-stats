package com.cabify.driver.stats

import scala.concurrent.duration.Duration

package object domain {
  def intervalSeconds(duration: Duration): String = s"${duration.toSeconds} seconds"
  def intervalMillis(duration: Duration): String  = s"${duration.toMillis} milliseconds"
}
