package com.cabify.driver.stats

import org.apache.spark.sql.SparkSession

package object app {
  def sparkSession(app: String): SparkSession =
    SparkSession
      .builder()
      .appName(app)
      .master("local[*]")
      .config("spark.sql.session.timeZone", "UTC")
      .config("spark.sql.streaming.statefulOperator.checkCorrectness.enabled", "false")
      .getOrCreate()
}
