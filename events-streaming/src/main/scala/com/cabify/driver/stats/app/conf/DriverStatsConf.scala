package com.cabify.driver.stats.app.conf

import scala.concurrent.duration.Duration

case class DriverStatsConf(
    topic: String,
    intervalDuration: Duration,
    mode: String
)
