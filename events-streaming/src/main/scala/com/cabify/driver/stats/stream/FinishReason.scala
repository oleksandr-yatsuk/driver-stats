package com.cabify.driver.stats.stream

import cabify.input.FinishReason.{DRIVER_CANCEL, DROP_OFF}

object FinishReason {
  val dropOff: String      = DROP_OFF.name
  val driverCancel: String = DRIVER_CANCEL.name
}
