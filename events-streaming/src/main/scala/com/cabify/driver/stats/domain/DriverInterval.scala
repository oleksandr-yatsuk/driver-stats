package com.cabify.driver.stats.domain

abstract class DriverInterval(val window: DriverStatisticsInterval, val driverId: String)
