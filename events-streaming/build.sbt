ThisBuild / scalaVersion := dependencies.scalaVersion

ThisBuild / version      := "1.0.0"
ThisBuild / organization := "com.cabify"

lazy val root = (project in file("."))
  .settings(
    name := "driver-stats",
    libraryDependencies ++= dependencies.all
  )
  .enablePlugins(PackPlugin)
  .settings(
    packJarNameConvention := "full",
    packExcludeJars       := Seq("scala-.*\\.jar")
  )

//resolvers += "confluent" at "https://packages.confluent.io/maven/"
