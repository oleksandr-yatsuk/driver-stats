import sbt.{ModuleID, _}

object dependencies {
  val scalaVersion = "2.12.14"
  val sparkVersion = "3.4.1"

  private val tests: Seq[ModuleID] = Seq(
    "org.scalactic" %% "scalactic" % "3.2.16" % Test,
    "org.scalatest" %% "scalatest" % "3.2.16" % Test,
    "org.scalamock" %% "scalamock" % "5.2.0"  % Test
  )

  private val spark: Seq[ModuleID] = Seq(
    "org.apache.spark" %% "spark-core" % sparkVersion % Provided,
    "org.apache.spark" %% "spark-sql"  % sparkVersion % Provided
  )

  private val kafka: Seq[ModuleID] = Seq(
    "org.apache.spark" %% "spark-sql-kafka-0-10" % sparkVersion % Compile
  )

  private val protobuf: Seq[ModuleID] = Seq(
    "com.thesamet.scalapb" %% "scalapb-runtime"        % "0.11.13" % Compile,
    "com.thesamet.scalapb" %% "sparksql33-scalapb0_11" % "1.0.3"   % Compile
  )

  private val libs: Seq[ModuleID] = Seq(
    "com.github.dwickern" %% "scala-nameof" % "4.0.0" % Provided,
    "com.github.scopt"    %% "scopt"        % "4.1.0" % Compile
  )

  val all: Seq[ModuleID] = spark ++
    kafka ++
    protobuf ++
    libs ++
    tests
}
